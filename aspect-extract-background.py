import numpy as np
import cv2

cascade = cv2.CascadeClassifier('haarcascade_upperbody.xml')

imgPath = 'j1.jpeg'

img = cv2.imread(imgPath)
img_copy = img.copy()
img_copy1 = img.copy()
img_copy2=img.copy()
height, width = img.shape[:2]
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

body = cascade.detectMultiScale(
    gray,
    scaleFactor=1.01,
    minNeighbors=5,
    # minSize = (30,30),
    flags=cv2.CASCADE_SCALE_IMAGE
)
ll = []

for (x2, y2, w2, h2) in body:
    print(x2, y2, w2, h2)
    ll.append(x2)

    cv2.rectangle(img, (x2, y2), (x2 + w2, y2 + h2), (255, 255, 255), 2)

if ll == [] or len(ll) >= 3 or len(ll) == 1:
    mask = np.zeros(img.shape[:2],np.uint8)
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)

    #rect = (50,50,450,290)
    rect = (10,10,width-40,height-40)

    cv2.grabCut(img, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)
    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')

    img1 = img*mask2[:,:,np.newaxis]


    #--------------Masking the image and apply contours--------------------
    gray = cv2.cvtColor((img_copy - img1), cv2.COLOR_BGR2GRAY)  # Final image with foreground seperated.
    #del img

    ret,thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV) #  thresh is mask which highlights foreground.
    im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = np.array([cv2.contourArea(c) for c in contours])

    x,y,w,h = cv2.boundingRect(contours[np.argmax(cnts)]) # dimensions of maximum contour area are obtained.

    cv2.rectangle(img_copy,(0,y),(width,y+h),(0,255,255),2)
    #cv2.imshow("imm",img_copy)
    cv2.imwrite("rect_1_t1.jpg",img_copy)

    extrc_im = img_copy[y+2:h-2,0:width-2]
    #cv2.imshow("original",img)
    #cv2.imshow("extracted", extrc_im)
    cv2.imwrite("rect_t1.jpg",extrc_im)

    aspect_ratio=1.8 # 16:9

    im21=cv2.imread("rect_t1.jpg")
    p=(im21.shape[0])
    print("height of cropped one", p)
    q=(im21.shape[1])
    print("width  of cropped one",q)

    width = ( p*aspect_ratio)

    #print("width",width)

    #print("value of cropped width",p)
    height = (q/aspect_ratio)
    #print("height",height)


    p=int(width)
    q=int(height)

    cv2.rectangle(img,(x,y),(p,q),(255,0,255),2)
    cv2.imshow("original_1.jpg",img)
    cv2.waitKey(0)

ll.sort()

if len(ll) == 2:
    aspect_ratio1=1.8
    wi=[ll[1]+w2]
    hi=[ll[1]+h2]
    width2 = (wi[0] * aspect_ratio1)
    height2 = (hi[0] / aspect_ratio1)

    p1 = int(width2)
    q1 = int(height2)
    cv2.rectangle(img_copy2, (ll[0], 0), (p1, q1), (255, 0, 255), 2)
    cv2.imshow("FINAL_IMAGE.jpg", img_copy2)
cv2.waitKey(0)
cv2.destroyAllWindows()
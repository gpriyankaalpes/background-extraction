import cv2
# coding: utf-8
import math
import dlib
from imutils import face_utils
def direction(img):


    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    im = cv2.imread(img)
    size = im.shape
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale frame
    rects = detector(gray, 0)

    # loop over the face detections
    for rect in rects:
        # determine the facial landmarks for the face region, then
        # convert the facial landmark (x, y)-coordinates to a NumPy
        # array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        shape = shape[[30,8,36,45,48,54]] # refers to co-ordinates of nose,chin,eyes,mouth.

        # 2D image points. If you change the image, you need to change vector
        image_points = np.array([(x,y) for x,y in shape], dtype="double")

        # 3D model points.
        model_points = np.array([
            (0.0, 0.0, 0.0),  # Nose tip
            (0.0, -330.0, -65.0),  # Chin
            (-225.0, 170.0, -135.0),  # Left eye left corner
            (225.0, 170.0, -135.0),  # Right eye right corne
            (-150.0, -150.0, -125.0),  # Left Mouth corner
            (150.0, -150.0, -125.0)  # Right mouth corner

        ])

        # Camera internals

        focal_length = size[1]
        center = (size[1] / 2, size[0] / 2)
        camera_matrix = np.array(
            [[focal_length, 0, center[0]],
             [0, focal_length, center[1]],
             [0, 0, 1]], dtype="double"
        )

        #print "Camera Matrix :\n {0}".format(camera_matrix)

        dist_coeffs = np.zeros((4, 1))  # Assuming no lens distortion
        (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, image_points, camera_matrix,
                                                                      dist_coeffs,
                                                                      )

        #print "Rotation Vector:\n {0}".format(rotation_vector)
        #print "Translation Vector:\n {0}".format(translation_vector)

        # Project a 3D point (0, 0, 1000.0) onto the image plane.
        # We use this to draw a line sticking out of the nose

        (nose_end_point2D, jacobian) = cv2.projectPoints(np.array([(0.0, 0.0, 1000.0)]), rotation_vector,
                                                         translation_vector,
                                                         camera_matrix, dist_coeffs)

        for p in image_points:
            cv2.circle(im, (int(p[0]), int(p[1])), 3, (0, 255, 255), -1)

        p1 = (int(image_points[0][0]), int(image_points[0][1]))
        p2 = (int(nose_end_point2D[0][0][0]), int(nose_end_point2D[0][0][1]))

        cv2.line(im, p1, p2, (255, 0, 0), 2)


        # x1,y1,x2,y2 are taken to calculate slope

        x1,y1 = p1
        x2,y2 = p2

        angle = math.atan2(y2 - y1, x2 - x1) * 180 / math.pi + 180

        if angle > 80 and angle <=95:
            return "straight"
        elif angle >260 and angle <=275:
            return "straight"
        elif (angle > 0 and angle <55) or (angle > 210 and angle <=360):
            return "left"
        elif(angle > 135 and angle < 180) or (angle >180 and angle <=225):
            return "right"


import numpy as np
import cv2

cascade = cv2.CascadeClassifier("haarcascade_upperbody.xml")

imgPath ="image123.JPG"

img = cv2.imread(imgPath)

img_copy1=img.copy()

height, width = img.shape[:2]
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


body = cascade.detectMultiScale(
    gray,
    scaleFactor = 1.01,
    minNeighbors = 5,
    #minSize = (30,30),
    flags = cv2.CASCADE_SCALE_IMAGE
)
ll=[]

for (x2, y2, w2, h2) in body:
    ll.append(x2)
    cv2.rectangle(img, (x2, y2), (x2+w2, y2+h2), (255, 255, 255), 2)



if ll==[] or len(ll)>=3 or len(ll)==1:

    mask = np.zeros(img_copy1.shape[:2], np.uint8)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    rect = (10, 10, width - 40, height - 40)

    cv2.grabCut(img_copy1, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype("uint8")

    img1 = img_copy1 * mask2[:, :, np.newaxis]

    # --------------Masking the image and apply contours--------------------
    gray = cv2.cvtColor((img_copy1 - img1), cv2.COLOR_BGR2GRAY)  # Final image with foreground seperated.

    ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV)  # thresh is mask which highlights foreground.
    im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnts = np.array([cv2.contourArea(c) for c in contours])

    x1, y1, w1, h1 = cv2.boundingRect(contours[np.argmax(cnts)])  # dimensions of maximum contour area are obtained.
    #cv2.rectangle(img_copy1, (x1, y1), (x1 + w1, y1 + h1), (0, 0, 255), 2)

    p=direction(imgPath)

    print p
    if(p=="right"):
        cv2.rectangle(img_copy1, (x1 + w1 + 10, 0), (width, height), (0, 255, 255), 2)
    elif (p=="left"):
        cv2.rectangle(img_copy1, (0, 0), (x1 - 10, height), (0, 255, 255), 2)
    elif(p!="right")or(p!="left")or(p!="up")or(p!="down") or(p=="straight"):
        f_space1 = x1

        f_space2 = width - (x1 + w1)


        if f_space1 > f_space2:

            cv2.rectangle(img_copy1, (0, 0), (x1 - 10, height), (0, 255, 255), 2)
        else:

            cv2.rectangle(img_copy1, (x1 + w1 + 10, 0), (width, height), (0, 255, 255), 2)
    #cv2.imshow("img",img_copy1)
    cv2.imwrite("img.jpg",img_copy1)

ll.sort()

if len(ll)==2:
    p = direction(imgPath)

    if (p == "right"):
        cv2.rectangle(img_copy1, (ll[1] + w2 + 10, 0), (width, height), (0, 255, 255), 2)
    elif (p=="left"):
        cv2.rectangle(img_copy1, (0, 0), (ll[0] - 10, height), (0, 255, 255), 2)
    elif(p!="right")or(p!="left")or(p!="up")or(p!="down"):
        f_space1 = ll[0]
        f_space2 = width - (ll[1] + w2)

        print(f_space1, f_space2)

        if f_space1 > f_space2:

            cv2.rectangle(img_copy1, (0, 0), (ll[0] - 10, height), (0, 255, 255), 2)
        else:

            cv2.rectangle(img_copy1, (ll[1] + w2 + 10, 0), (width, height), (0, 255, 255), 2)
        #cv2.imshow("im",img_copy2)
    cv2.imshow("FINAL_IMAGE.jpg", img_copy1)
cv2.waitKey(0)
cv2.destroyAllWindows()

import numpy as np
import cv2

cascade = cv2.CascadeClassifier('haarcascade_upperbody.xml')

imgPath = 'Leonardo.jpg'

img = cv2.imread(imgPath)
img_copy =img.copy()
img_copy1=img.copy()
height, width = img.shape[:2]
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


body = cascade.detectMultiScale(
    gray,
    scaleFactor = 1.01,
    minNeighbors = 5,
    #minSize = (30,30),
    flags = cv2.CASCADE_SCALE_IMAGE
)
ll=[]

for (x2, y2, w2, h2) in body:
    print(x2,y2,w2,h2)
    ll.append(x2)


    cv2.rectangle(img, (x2, y2), (x2+w2, y2+h2), (255, 255, 255), 2)



if ll==[] or len(ll)>3 or len(ll)==1:

    mask = np.zeros(img_copy1.shape[:2], np.uint8)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    rect = (10, 10, width - 40, height - 40)

    cv2.grabCut(img_copy1, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')

    img1 = img_copy1 * mask2[:, :, np.newaxis]

    # --------------Masking the image and apply contours--------------------
    gray = cv2.cvtColor((img_copy1 - img1), cv2.COLOR_BGR2GRAY)  # Final image with foreground seperated.

    ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV)  # thresh is mask which highlights foreground.
    im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnts = np.array([cv2.contourArea(c) for c in contours])

    x1, y1, w1, h1 = cv2.boundingRect(contours[np.argmax(cnts)])  # dimensions of maximum contour area are obtained.
    cv2.rectangle(img_copy1, (x1, y1), (x1 + w1, y1 + h1), (0, 0, 255), 2)
    f_space1 = x1
    f_space2 = width - (x1 + w1)



    if f_space1 > f_space2:

        cv2.rectangle(img_copy1, (0, 0), (x1 - 10, height), (0, 255, 255), 2)
    else:

        cv2.rectangle(img_copy1, (x1 + w1 + 10, 0), (width, height), (0, 0, 255), 2)
    cv2.imwrite("image",img_copy1)

ll.sort()

if len(ll)==2:


    f_space1 = ll[0]
    f_space2 = width - (ll[1]+w2)




    if f_space1 > f_space2:

        cv2.rectangle(img, (0, 0), (ll[0] - 10, height), (0, 255, 255), 2)
    else:

        cv2.rectangle(img, (ll[1]+w2+10, 0), (width, height), (0, 0, 255), 2)




cv2.imwrite("FINAL_IMAGE.jpg", img)
cv2.waitKey(0)
cv2.destroyAllWindows()